import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
from torch.autograd import Variable

class _cnn(nn.Module):
    def __init__(self):
        super(_cnn, self).__init__()
        self.conv1 = nn.Conv1d(1, 20, 5)
        self.conv2 = nn.Conv1d(20, 50, 5)
        self.fc1 = nn.Linear(1*50*7, 500)
        self.fc2 = nn.Linear(500, 2)

    def forward(self, x):
        # print("before in conv1", x.shape)
        x = self.conv1(x)
        x = F.relu(x)
        # print("after conv1", x.shape)
        x = self.conv2(x)
        x = F.relu(x)
        # print("after conv2", x.shape)
        x = x.view(-1, 1*50*7)
        x = self.fc1(x)
        x = F.relu(x)
        x = self.fc2(x)

        return x  # F.log_softmax(x, dim=1)


def main():
    # for Test
    a = np.random.rand(1, 1, 15) # batch, data
    a = torch.from_numpy(a).float()
    # print(a)
    cnn_model = _cnn()
    out = cnn_model(a)
    pred = out.max(1, keepdim=True)[1]
    # target = np.array([1])
    # print("target:", target)
    # target = torch.from_numpy(target)
    # criterion = nn.CrossEntropyLoss()
    # loss = criterion(pred, Variable(target.long()))

    print(out)
    print(pred)
    if (pred == 1):
        print("***correct***")
    else:
        print("***wrong***")

if __name__ == "__main__":
    main()