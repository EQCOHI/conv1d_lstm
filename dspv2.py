import matplotlib.pyplot as plt
from scipy.interpolate import splrep, splev
import pandas as pd
import numpy as np
import math

# data load
file = "./data/fft.csv"
signal = pd.read_csv(file)

fs = 1000   # sampling frequency
dt = 1/fs  # 0.001

newt = np.arange(0, 1, dt)
newx = signal

# 주파수 생성
nfft = len(newt)    # number of sample count
df = fs/nfft        # 주파수 증가량
k = np.arange(nfft)
f = k * df          # 0 ~ Maximum 주파수

nfft_half = math.trunc(nfft/2) # 대칭이니까 절반만 구함
f0 = f[range(nfft_half)] # only half size check for get hz.
y = np.fft.fft(newx)/nfft * 2 # 증폭을 두 배로 한다. (반만 계산해서 에너지가 반으로 줌)
y0 = y[range(nfft_half)]
amp = abs(y0)                 # magnitude


plt.subplot(3,1,1)
plt.xlabel("Time domain")
plt.plot(newx)
plt.grid()

plt.subplot(3,1,3)
plt.xlabel("Frequency domain(Hz)")
plt.plot(f0, amp)
plt.grid()
plt.show()