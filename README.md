# KISTI project in IS LAB


# Usage
- This project can use plenty of model(CNN(1d), LSTM, CNN-LSTM) in time series data 
- python main.py --model cnn(or lstm or cnn_lstm) 

# version
- pytorch 1.1.0 

# Accuracy
Model | Accuracy

CNN   |   %

LSTM  |   %

CNN-LSTM | %   


