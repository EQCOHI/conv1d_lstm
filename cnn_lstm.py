import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np


class _cnn_lstm(nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim, layers):
        super(_cnn_lstm, self).__init__()
        self.conv1 = nn.Conv1d(1, 20, 5)
        self.conv2 = nn.Conv1d(20, 50, 5)
        # 작업중..
        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers=layers, batch_first=True)
        self.fc = torch.nn.Linear(hidden_dim, output_dim, bias=True)

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)

        x = self.conv2(x)
        x = F.relu(x)

        x = x.view(-1, 1*50*7)
        # print("cnn_lstm shape: ", x.shape)
        x = x.reshape(64, 1, 350)
        # print("cnn_lstm shape: ",x.shape)
        x, _status = self.lstm(x)
        x = self.fc(x[:, -1])

        # print(x.shape)
        # exit()
        return(x)

if __name__ == "__main__":
    # for Test
    a = np.random.rand(1, 1, 15) # batch, data
    a = torch.from_numpy(a).float()
    # print(a)

    # do  test
    model = _cnn_lstm(350, 50, 2, 1)
    out = model(a)
    print(out)


    # pred = out.max(1, keepdim=True)[1]

    # print(out)
    # print(pred)
    # if (pred == 1):
    #     print("***correct***")
    # else:
    #     print("***wrong***")
    #
