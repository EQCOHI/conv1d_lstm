import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np

# Setting
#input_dim: 15, hidden_dim: random, output_dim:1, layers:1

class _lstm(torch.nn.Module):
    def __init__(self, input_dim, hidden_dim, output_dim, layers):
        super(_lstm, self).__init__()
        self.lstm = torch.nn.LSTM(input_dim, hidden_dim, num_layers=layers, batch_first=True)
        self.fc = torch.nn.Linear(hidden_dim, output_dim, bias=True)

    def forward(self, x):
        print("lstm x shape: ", x.shape)
        x, _status = self.lstm(x)
        x = self.fc(x[:, -1])
        return x
