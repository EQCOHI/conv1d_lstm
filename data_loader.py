from torch.utils.data import Dataset, DataLoader
import torch
import os
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler

root_dir = "C:\\Users\\USER\\PycharmProjects\\kisti"
train_file = os.path.join(root_dir, "train.csv")
test_file = os.path.join(root_dir, "test.csv")
min_max_scaler = MinMaxScaler()

class kisti_data_loader(Dataset):

    def __init__(self, csv_file):
        self.data = pd.read_csv(csv_file)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        self.data = np.array(self.data)

        x = self.data[idx, 0:15]
        x = x.reshape(1,15)
        # print(x)
        # x = min_max_scaler.fit_transform(x)
        # print(x.shape)
        y = self.data[idx, -1]
        # print(x)
        # print(y)
        x = torch.from_numpy(np.array(x)).float()
        y = torch.from_numpy(np.array(y)).long()

        sample = {"X": x, "Y": y}

        return sample
