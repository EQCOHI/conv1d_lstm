import argparse
from torch.utils.data import Dataset, DataLoader
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import torch.nn.functional as F
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from cnn import _cnn
from lstm import _lstm
from cnn_lstm import _cnn_lstm

from data_loader import kisti_data_loader

train_file = "./data/train.csv"
test_file = "./data/test.csv"
criterion = nn.CrossEntropyLoss()
train_loss_for_graph = []
test_loss_for_graph = []
accuracy_list = []
test_accuracy_graph = []


def train(args, model, device, train_loader, optimizer, epoch):
    criterion.to(device)

    model.train()
    use_gpu = torch.cuda.is_available()

    for batch_idx, batch in enumerate(train_loader):
        # if use_gpu:
        #     data = Variable(batch['X'].cuda())
        #     target = Variable(batch['Y'].cuda())
        # else:
        #     data, target = Variable(batch['X']), Variable(batch['Y'])
        data = batch['X']
        target = batch['Y']
        data, target = data.to(device), target.to(device)

        optimizer.zero_grad()
        pred = model(data)
        # pred = pred.max(1, keepdim=True)[1]
        # print("pred: ", pred, type(pred))
        # print("target: ", target, type(target))
        loss = criterion(pred, target)

        loss.backward()
        optimizer.step()
        # train_loss_for_graph.append(loss.item())

        print('\nTrain Epoch: {}[{}/{} ({:.0f}%)]\tLoss:{:.6f}'.format(epoch, batch_idx * len(data), len(train_loader.dataset), 100.*batch_idx/len(train_loader), loss.item()))

        return loss.item()

def test(args, model, device, test_loader):
    model.eval()
    test_loss = 0
    use_gpu = torch.cuda.is_available()
    correct = 0
    test_total = 0

    with torch.no_grad():
        for batch_idx, batch in enumerate(test_loader):
            if use_gpu:
                data = Variable(batch['X'].cuda())
                target = Variable(batch['Y'].cuda())
            else:
                data, target = Variable(batch['X']), Variable(batch['Y'])
            data, target = data.to(device, dtype= torch.float), target.to(device, dtype=torch.long)

            output = model(data)

            # print("test_pred: ", pred)
            # print("test_target: ", target)

            # loss
            test_loss = criterion(output, target).item()

            # For accuracay
            pred = output.max(1, keepdim=True)[1]

            correct += pred.eq(target.view_as(pred)).sum().item()
            test_total += target.size(0)
            Accuracy = 100. * correct / test_total
            test_accuracy_graph.append(Accuracy)
            # test_loss_for_graph.append(test_loss)
            print("Test Set: Average Loss:{:.4f}, Accuracy: ({:.0f}%)\n".format(test_loss, Accuracy))

            return test_loss

def main():
    # Training Setting
    parser = argparse.ArgumentParser(description="kisti research")
    parser.add_argument('--model', type=str, default='cnn_lstm', choices=['cnn', 'lstm', 'cnn_lstm'])
    parser.add_argument('--batch-size', type=int, default=64, metavar='N', help='input batch size(default:8)')
    parser.add_argument('--epochs', type=int, default=200, metavar='N', help='number of epochs for train')
    parser.add_argument('--lr', type=float, default=0.01, metavar="LR", help='learning rate(default=0.01)')
    parser.add_argument('--seed', type=int, default=7, metavar='S', help='random seed(default:1)')
    parser.add_argument('--save-model', action='store_true', default=False, help='For saving trained model')
    parser.add_argument('--no-cuda', action='store_true', default=False, help='disable CUDA training')

    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()
    torch.manual_seed(args.seed)
    device = torch.device("cuda" if use_cuda else "cpu")

    # data load
    train_data = kisti_data_loader(csv_file=train_file)
    test_data = kisti_data_loader(csv_file=test_file)

    # show a batch
    batch_size = 4
    for i in range(batch_size):
        sample = train_data[i]
        print(i, sample['X'].size, sample['Y'].size)

    # create data loader
    train_loader = DataLoader(train_data, batch_size=args.batch_size, shuffle=True, num_workers=4)
    test_loader = DataLoader(test_data, batch_size=args.batch_size, shuffle=True, num_workers=4)

    # model & optimizer
    if args.model == 'cnn':
        model = _cnn().to(device)
    elif args.model == 'lstm':
        model = _lstm(input_dim=15, hidden_dim=10, output_dim=2, layers=1).to(device)
    elif args.model == 'cnn_lstm':
        # if you use cnn_lstm, check batch size in Class cnn_lstm
        model = _cnn_lstm(input_dim=350, hidden_dim=10, output_dim=2, layers=1)

    optimizer = optim.SGD(model.parameters(), lr=args.lr)

    # Do train & test
    for epoch in range(1, args.epochs +1):
        tr_loss = train(args, model, device, train_loader, optimizer, epoch)
        ts_loss = test(args, model, device, test_loader)

        if epoch % 2 == 0:
            train_loss_for_graph.append(tr_loss)
            test_loss_for_graph.append(ts_loss)

    # Show Result to Graph
    plt.figure(1)
    # plt.axis([None, None, 0, 1]) # range ->  x: none, 0 < y < 1
    plt.xlim(0, 100)
    plt.ylim(0, 1)
    plt.title("Train Loss for {}".format(args.model))
    plt.xlabel("Epoch per 2")
    plt.ylabel("Loss")

    plt.plot(train_loss_for_graph, color='red', label='train_loss')
    plt.plot(test_loss_for_graph, color='blue', label='test_loss')

    plt.legend(['train_loss', 'test_loss'])
    plt.show()

    # show accuracy
    plt.figure(2)
    plt.ylim(0,100)
    plt.title("Accuracy for {}".format(args.model))
    plt.ylabel("Accuracy")
    plt.xlabel("Epoch")

    plt.plot(test_accuracy_graph, color="yellow", label="test_accuracy")
    plt.legend(['Test_Accuracy'])
    plt.show()

if __name__ == "__main__":
    main()
